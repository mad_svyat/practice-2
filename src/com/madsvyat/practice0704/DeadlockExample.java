package com.madsvyat.practice0704;

/**
 *
 */
public class DeadlockExample {

    public static void main(String[] args) {

        final Object firstResource = new Object();
        final Object secondResource = new Object();

        Thread first = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + " started");

            synchronized (firstResource) {
                System.out.println(Thread.currentThread().getName() +
                        " in first synchronized block");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (secondResource) {
                    System.out.println(Thread.currentThread().getName() +
                            " in second synchronized block");
                }
            }
            System.out.println(Thread.currentThread().getName() + " finished");
        });

        Thread second = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + " started");

            synchronized (secondResource) {
                System.out.println(Thread.currentThread().getName() +
                        " in first synchronized block");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (firstResource) {
                    System.out.println(Thread.currentThread().getName() +
                            " in second synchronized block");
                }
            }
            System.out.println(Thread.currentThread().getName() + " finished");
        });

        first.start();
        second.start();

        try {
            first.join();
            second.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

