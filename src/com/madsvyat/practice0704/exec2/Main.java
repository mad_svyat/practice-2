package com.madsvyat.practice0704.exec2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Main {

    public static void main(String[] args) {
        int a[] = { 2, 67, 39};
        int b[] = {23, 12, 45};
        int c[] = {56, 87, 45};

        Consumer consumer = new Consumer();
        List<Thread> threads = new ArrayList<>();

        for (int aVal : a) {
            threads.add(new Thread(new Cubator(aVal, consumer)));
        }

        for (int bVal : b) {
            threads.add(new Thread(new Kvadrator(bVal, consumer)));
        }

        for (int cVal : c) {
            threads.add(new Thread(new Simple(cVal, consumer)));
        }

        threads.forEach(Thread::start);
    }
}
