package com.madsvyat.practice0704.exec2;


/**
 *
 */
public class Consumer {

    private final Semaphore semaphore = new Semaphore();

    public void sum(int cube, int kvadro, int simple) {

        synchronized (semaphore) {

            if (cube != 0) {
                while (semaphore.hasCubatorFlag()) {
                    Thread.yield();
                }
                semaphore.setCubatorFlag(true);

            } else if (kvadro != 0) {
                while (semaphore.hasKvadratorFlag()) {
                    Thread.yield();
                }
                semaphore.setKvadratorFlag(true);
            } else if (simple != 0) {
                while (semaphore.hasSimpleFlag()) {
                    Thread.yield();
                }
                semaphore.setSimpleFlag(true);
            }

            System.out.println("result: " + (cube + kvadro + simple));

            if (cube != 0) {
                semaphore.setCubatorFlag(false);
            }

            if (kvadro != 0) {
                semaphore.setKvadratorFlag(false);
            }

            if (simple != 0) {
                semaphore.setSimpleFlag(false);
            }

        }
    }
}
