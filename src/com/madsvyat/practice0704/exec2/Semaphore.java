package com.madsvyat.practice0704.exec2;




/**
 *
 */
public class Semaphore {

    private volatile boolean cubatorFlag;
    private volatile boolean kvadratorFlag;
    private volatile boolean simpleFlag;

    public void setCubatorFlag(boolean cubatorFlag) {
        this.cubatorFlag = cubatorFlag;
    }

    public void setKvadratorFlag(boolean kvadratorFlag) {
        this.kvadratorFlag = kvadratorFlag;
    }

    public void setSimpleFlag(boolean simpleFlag) {
        this.simpleFlag = simpleFlag;
    }

    public boolean hasCubatorFlag() {
        return cubatorFlag;
    }

    public boolean hasKvadratorFlag() {
        return kvadratorFlag;
    }

    public boolean hasSimpleFlag() {
        return simpleFlag;
    }

    public boolean hasAccess() {
        return cubatorFlag && kvadratorFlag && simpleFlag;
    }
}
