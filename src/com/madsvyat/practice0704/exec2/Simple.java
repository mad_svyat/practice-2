package com.madsvyat.practice0704.exec2;

/**
 *
 */
public class Simple implements Runnable {

    private int value;
    private Consumer consumer;

    public Simple(int value, Consumer consumer) {
        this.value = value;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        consumer.sum(0, 0, value);
    }
}
