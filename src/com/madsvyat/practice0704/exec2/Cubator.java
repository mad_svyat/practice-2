package com.madsvyat.practice0704.exec2;

/**
 *
 */
public class Cubator implements Runnable {

    private int value;
    private Consumer consumer;

    public Cubator(int value, Consumer consumer) {
        this.value = value;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        value = value * value * value;
        consumer.sum(value, 0, 0);
    }
}
