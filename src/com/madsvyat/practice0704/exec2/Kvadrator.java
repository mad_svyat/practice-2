package com.madsvyat.practice0704.exec2;

/**
 *
 */
public class Kvadrator implements Runnable {

    private int value;
    private Consumer consumer;

    public Kvadrator(int value, Consumer consumer) {
        this.value = value;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        value = value * value;
        consumer.sum(0, value, 0);
    }
}
