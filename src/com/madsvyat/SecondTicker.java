package com.madsvyat;

/**
 *
 */
public class SecondTicker implements Runnable {

    private final Object monitor;

    public SecondTicker(Object monitor) {
        this.monitor = monitor;
    }

    @Override
    public void run() {

        while (true) {
            try {
                Thread.sleep(1000);
                synchronized (monitor) {
                    monitor.notifyAll();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
