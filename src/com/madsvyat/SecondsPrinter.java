package com.madsvyat;

/**
 *
 */
public class SecondsPrinter implements Runnable {

    private int interval;
    private int count;
    private final Object monitor;

    public SecondsPrinter(Object monitor, int interval) {
        this.interval = interval;
        this.monitor = monitor;
    }

    @Override
    public void run() {

        try {
            while (true) {
                count++;
                if (count % interval == 0) {
                    System.out.println("tick from " +
                            Thread.currentThread().getName() + ": " + count);
                } else {
                    synchronized (monitor) {
                        monitor.wait();
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
