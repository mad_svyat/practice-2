package com.madsvyat;

public class Main {

    public static void main(String[] args) {

        final Object monitor = new Object();

        Thread tickerThread = new Thread(new SecondTicker(monitor));
        Thread fiveSecondsWriter = new Thread(new SecondsPrinter(monitor, 5));
        Thread sevenSecondsWriter = new Thread(new SecondsPrinter(monitor, 7));

        tickerThread.start();
        fiveSecondsWriter.start();
        sevenSecondsWriter.start();

        try {
            tickerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
